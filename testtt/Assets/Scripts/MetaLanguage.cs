﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MetaLanguage : MonoBehaviour
{
    public Text m_Text;
    public Text t_Text;
    public List<char> randomSymbols = new List<char> { '?', '>', '<', ';', ':', '-', '_', '=', '+', '$', '%', '#', '@', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '(', ')', '!', '*', '&', '|', '\\' };
    public char[] bgAlphabet = { 'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь', 'ю', 'я' };


    void Start()
    {
        Generate();
    }
    private void Generate()
    {
        Dictionary<char, char> bgToRand = new Dictionary<char, char>();

        foreach (char letter in bgAlphabet)
        {
            int mIndex = UnityEngine.Random.Range(0, randomSymbols.Count);
            char randomLetter = randomSymbols.ElementAt(mIndex);
            randomSymbols.RemoveAt(mIndex);
            bgToRand[letter] = randomLetter;
        }
        m_Text.text = "";
        t_Text.text = "";
        foreach (var pair in bgToRand)
        {
            var value = pair.Key.ToString();
            m_Text.text += pair.Key.ToString() + "\r\n";
            t_Text.text += pair.Value.ToString() + "\r\n";
        }

    }
}



