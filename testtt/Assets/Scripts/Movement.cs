﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    public float movingSpeed = 10;
    public float turningSpeed =60;
    public Animator anim;

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal") * turningSpeed * Time.deltaTime;
        transform.Rotate(0, horizontal, 0);
        float vertical = Input.GetAxis("Vertical") * movingSpeed * Time.deltaTime;
        transform.Translate(0, 0, vertical);
        float lol = vertical;
        anim.SetFloat("Speed", lol);
    }
}
