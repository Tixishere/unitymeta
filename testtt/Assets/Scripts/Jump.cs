﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {

    public Rigidbody rb;
    public float jumpSpeed = 5f;
 

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
 
	}
	
	// Update is called once per frame
	void Update () {
        if  (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);

        }
    }
     
}
