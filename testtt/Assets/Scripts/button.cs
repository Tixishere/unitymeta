﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class button : MonoBehaviour {
  
 
    public GameObject Meta;
    public GameObject Main;
    public GameObject resolutionDropdown;
    public GameObject Options;
    public bool isMainNotHidden = true;
    public bool isMetaNotHidden;
    public bool isOptionsNotHidden;
    // Use this for initialization
    void Start () {

        Options.SetActive(isOptionsNotHidden);
        Meta.SetActive(isMetaNotHidden);
        Main.SetActive(isMainNotHidden);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Meta_Menu"))
        {

            isMetaNotHidden = !isMetaNotHidden;
            isMainNotHidden = !isMainNotHidden;
            Meta.SetActive(isMetaNotHidden);
            Main.SetActive(isMainNotHidden);
        }
        if (Input.GetButtonDown("Options_Menu"))
        {
            isOptionsNotHidden = !isOptionsNotHidden;
            isMainNotHidden = !isMainNotHidden;
            Main.SetActive(isMainNotHidden);
            Options.SetActive(isOptionsNotHidden);

        }
    }
    
}
