﻿using UnityEngine;

public class Camera : MonoBehaviour {

    public GameObject target;
    Vector3 offset;

    void Start()
    {
        offset = target.transform.position - transform.position;
    }
	
	
	// Update is called once per frame
	void LateUpdate () {
        float angle = target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, angle, 0);
        transform.position = target.transform.position - (rotation * offset);
        transform.LookAt(target.transform);

	}
}
